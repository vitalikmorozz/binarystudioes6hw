import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';

export async function fight(firstFighter, secondFighter) {
	return new Promise((resolve) => {
		// resolve the promise with the winner when fight is over
		let leftFighterHealthBar = document.getElementById('left-fighter-indicator');
		let rightFighterHealthBar = document.getElementById('right-fighter-indicator');
		const leftFighterFullHealthBarWidth = leftFighterHealthBar.clientWidth;
		const rightFighterFullHealthBarWidth = rightFighterHealthBar.clientWidth;

		const firstFighterFullHealth = firstFighter.health;
		const secondFighterFullHealth = secondFighter.health;

		const updateHealthBars = (firstFighterCurrentHealth, secondFighterCurrentHealth) => {
			if (firstFighterCurrentHealth != firstFighterFullHealth)
				leftFighterHealthBar.style.width = (leftFighterFullHealthBarWidth / firstFighterFullHealth) * firstFighterCurrentHealth + 'px';
			if (secondFighterCurrentHealth != secondFighterFullHealth)
				rightFighterHealthBar.style.width = (rightFighterFullHealthBarWidth / secondFighterFullHealth) * secondFighterCurrentHealth + 'px';
		};

		let firstPlayerInBlock = false;
		let secondPlayerInBlock = false;

		let firstFighterSuper = true;
		let secondFighterSuper = true;

		let keysPressed = {};

		document.addEventListener('keydown', handleKeyDown);

		function handleKeyDown(event) {
			const currentKey = event.code;
			keysPressed[event.code] = true;

			//Handle first fighter super
			if (
				keysPressed[controls.PlayerOneCriticalHitCombination[0]] &&
				keysPressed[controls.PlayerOneCriticalHitCombination[1]] &&
				keysPressed[controls.PlayerOneCriticalHitCombination[2]] &&
				secondFighterSuper
			) {
				firstFighter.health -= 2 * secondFighter.attack;
				secondFighterSuper = false;
				updateHealthBars(firstFighter.health, secondFighter.health);

				setTimeout(() => {
					secondFighterSuper = true;
				}, 10000);
			}

			//Handle second fighter super
			if (
				keysPressed[controls.PlayerTwoCriticalHitCombination[0]] &&
				keysPressed[controls.PlayerTwoCriticalHitCombination[1]] &&
				keysPressed[controls.PlayerTwoCriticalHitCombination[2]] &&
				firstFighterSuper
			) {
				secondFighter.health -= 2 * firstFighter.attack;
				firstFighterSuper = false;
				updateHealthBars(firstFighter.health, secondFighter.health);
				setTimeout(() => {
					firstFighterSuper = true;
				}, 10000);
			}

			//Handle simple attacks and blocks
			switch (currentKey) {
				//Handle first player attacks
				case controls.PlayerOneAttack: {
					if (firstPlayerInBlock) break;
					if (secondPlayerInBlock) secondFighter.health -= getDamage(firstFighter, secondFighter);
					if (!secondPlayerInBlock) secondFighter.health -= getHitPower(firstFighter);
					updateHealthBars(firstFighter.health, secondFighter.health);
					if (secondFighter.health <= 0) {
						showWinnerModal(firstFighter);
						document.removeEventListener('keydown', handleKeyDown);
						document.addEventListener('keyup', handleKeyUp);
						resolve();
					}
					break;
				}

				//Handle second player attacks
				case controls.PlayerTwoAttack: {
					if (secondPlayerInBlock) break;
					if (firstPlayerInBlock) firstFighter.health -= getDamage(secondFighter, firstFighter);
					if (!firstPlayerInBlock) firstFighter.health -= getHitPower(secondFighter);
					updateHealthBars(firstFighter.health, secondFighter.health);
					if (firstFighter.health <= 0) {
						showWinnerModal(secondFighter);
						document.removeEventListener('keydown', handleKeyDown);
						document.addEventListener('keyup', handleKeyUp);
						resolve();
					}
					break;
				}

				//First player block state  change
				case controls.PlayerOneBlock: {
					if (firstPlayerInBlock) break;
					firstPlayerInBlock = true;
					break;
				}

				//Second player block state change
				case controls.PlayerTwoBlock:
					if (secondPlayerInBlock) break;
					secondPlayerInBlock = true;
					break;
			}
		}

		document.addEventListener('keyup', handleKeyUp);

		function handleKeyUp(event) {
			const currentKey = event.code;
			delete keysPressed[event.code];
			switch (currentKey) {
				//First player block state  change
				case controls.PlayerOneBlock: {
					firstPlayerInBlock = false;
					break;
				}

				//Second player block state change
				case controls.PlayerTwoBlock:
					secondPlayerInBlock = false;
					break;
			}
		}
	});
}

export function getDamage(attacker, defender) {
	// return damage
	const damage = getHitPower(attacker) - getBlockPower(defender);
	if (damage < 0) return 0;
	return damage;
}

export function getHitPower(fighter) {
	// return hit power
	const hitPower = fighter.attack * (1 + Math.random());
	return hitPower;
}

export function getBlockPower(fighter) {
	// return block power
	const blockPower = fighter.defense * (1 + Math.random());
	return blockPower;
}
