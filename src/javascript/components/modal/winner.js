import { showModal } from './modal';

export function showWinnerModal(fighter) {
	// call showModal function
	showModal({ title: `${fighter.name} Wins`, bodyElement: `What a fight!` });
}
