import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
	const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
	const fighterElement = createElement({
		tagName: 'div',
		className: `fighter-preview___root ${positionClassName}`,
	});

	// todo: show fighter info (image, name, health, etc.)
	const { source, name } = fighter;
	const attributes = {
		src: source,
		title: name,
		alt: name,
	};

	const fighterImage = createElement({
		tagName: 'img',
		className: `fighter-preview___img ${positionClassName}`,
		attributes,
	});

	fighterElement.appendChild(fighterImage);
	return fighterElement;
}

export function createFighterImage(fighter) {
	const { source, name } = fighter;
	const attributes = {
		src: source,
		title: name,
		alt: name,
	};
	const imgElement = createElement({
		tagName: 'img',
		className: 'fighter-preview___img',
		attributes,
	});

	return imgElement;
}
